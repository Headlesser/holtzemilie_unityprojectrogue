﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public int pointsPerKey = 1;
	public int pointsPerTorch = 1;
	public float restartLevelDelay = 1f;
	public Text foodText;
	public Text keyText;
	public Text timerText;
	public AudioClip moveSound1;
	public AudioClip moveSound2;
	public AudioClip eatSound1;
	public AudioClip eatSound2;
	public AudioClip drinkSound1;
	public AudioClip drinkSound2;
	public AudioClip gameOverSound;
	public RectTransform spotlight;
	public Camera cam;
	public Sprite doorOpen;
	public float startTime = 15f;

	private Animator animator;
	private int food;
	private int keys;
	private int keyNum;
	private int torch;
	private int torchNum;

	// Use this for initialization
	protected override void Start () 
	{
		animator = GetComponent<Animator> ();

		keyNum = GameManager.instance.keyCount;
		food = GameManager.instance.playerFoodPoints;
		keys = GameManager.instance.playerKeyPoints;
		torchNum = GameManager.instance.torchCount; // = 1
		torch = GameManager.instance.playerTorches; // = 0
		//When I uncomment this text line, the player suddenly cannot move?
		keyText.text = "Keys: " + keys;
		foodText.text = "Health: " + food;

		base.Start ();
	}

	private void OnDisable()
	{
		GameManager.instance.playerFoodPoints = food;
	}

	// Update is called once per frame
	void Update () 
	{
		startTime -= Time.deltaTime;
		timerText.text = "Countdown: " + (int)startTime;
		CheckIfGameOver ();

		if (!GameManager.instance.playersTurn) return;
		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0)
			vertical = 0;

		if (horizontal != 0 || vertical != 0)
			AttemptMove<Wall> (horizontal, vertical);
		
		UpdateSpotlight ();
	}


	void UpdateSpotlight()
	{
		//fixes the spotlight positioning
		Vector3 screenPos = cam.WorldToScreenPoint(transform.position);
		spotlight.anchoredPosition = new Vector3 ((1f * screenPos.x) - Screen.width*.5f, (1f * screenPos.y) - Screen.height*.5f, 0f);
	}

	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		//Player loses 1 food point every time they move
		//food--;
		foodText.text = "Health: " + food;

		base.AttemptMove <T> (xDir, yDir);

		RaycastHit2D hit;
		if (Move (xDir, yDir, out hit)) 
		{
			SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		}

		CheckIfGameOver ();

		GameManager.instance.playersTurn = false;
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Exit" && keys == keyNum) {
			//Change exit tile sprite to opendoor
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		}

		else if (other.tag == "Torch"){
			torch += pointsPerTorch;
			other.gameObject.SetActive (false);
			if (torch == torchNum) {
				spotlight.GetComponent<RectTransform> ().sizeDelta = new Vector2 (500f, 500f);
			}
		}
		
		else if (other.tag == "Key"){
			keys += pointsPerKey;
			keyText.text = "+" + pointsPerKey + " Keys: " + keys;
			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
			other.gameObject.SetActive (false);
			if (keys == keyNum) 
			{
				GameManager.instance.Exit.GetComponent<SpriteRenderer>().sprite = doorOpen;
			}
			}

		 else if (other.tag == "Food") {
			food += pointsPerFood;
			foodText.text = "+" + pointsPerFood + " Health: " + food;
			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
			other.gameObject.SetActive (false);

		} else if (other.tag == "Soda") 
		{
			food += pointsPerSoda;
			foodText.text = "+" + pointsPerSoda + " Health: " + food;
			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);
			other.gameObject.SetActive (false);
		}
	}

	protected override void OnCantMove <T> (T component)
	{
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("playerChop");
	}

	private void Restart()
	{
		SceneManager.LoadScene (0);
	}

	public void LoseFood (int loss)
	{
		animator.SetTrigger ("playerHit");
		food -= loss;
		foodText.text = "-" + loss + " Health: " + food;
		CheckIfGameOver ();
	}

	private void CheckIfGameOver()
	{
		if (startTime < 0 || food <= 0) {
			//SoundManager.instance.PlaySingle (gameOverSound);
			//SoundManager.instance.musicSource.Stop ();
			GameManager.instance.GameOver ();
		}
	}
}