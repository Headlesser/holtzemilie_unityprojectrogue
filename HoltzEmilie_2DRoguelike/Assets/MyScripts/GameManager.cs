﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public float levelStartDelay = 2f;
	public float turnDelay = .1f;
	public float extraDelay = .01f;
	public static GameManager instance = null;
	public BoardManager boardScript;
	public int playerFoodPoints = 100;
	public int playerKeyPoints = 0;
	public int keyCount = 3;
	public int torchCount = 1;
	public int playerTorches = 0;
	public GameObject Exit;
	[HideInInspector] public bool playersTurn = true;

	private Text levelText;
	private GameObject levelImage;
	private int level = 0;
	private List<Enemy> enemies;
	private bool enemiesMoving;
	private bool doingSetup;
	private bool gameOver = false;



	// Use this for initialization
	void Awake () 
	{
		if (instance == null)
			instance = this;
		else if (instance != null) {
			Destroy (gameObject);
			return;
		}
		ResetValues ();
		DontDestroyOnLoad (gameObject);
		enemies = new List<Enemy> ();
		boardScript = GetComponent<BoardManager> ();
		if (gameOver) {
			InitGame ();
		}
	}

	void ResetValues(){
		level = 0;
		gameOver = false;
		playerFoodPoints = 100;
	}

	private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
		if (gameOver) {
			ResetValues ();
		}
	
		level++;
		InitGame();
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}



	void InitGame()
	{
		doingSetup = true;

		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();
		levelText.text = "Day " + level + "\n\n\n Find all 3 keys to escape!";
		levelImage.SetActive (true);
		Invoke ("HideLevelImage", levelStartDelay);

		enemies.Clear ();
		boardScript.SetupScene(level);
	}


	private void HideLevelImage()
	{
		levelImage.SetActive (false);
		doingSetup = false;
	}

	public void GameOver()
	{
		if (!gameOver) {
			levelText.text = "You died at room " + level + ".";
			levelImage.SetActive (true);
			//disable this line so the game resets properly.. may cause bugs. Check for the game over bool if needed.
			//enabled = false;
			//resets game
			gameOver = true;
			Invoke ("ReloadGameScene", 2f);
		}
	}
	//function called when it is time to reset game via Invoke method
	void ReloadGameScene(){
		Application.LoadLevel (Application.loadedLevel);
	}

	// Update is called once per frame
	void Update ()
	{
		if (playersTurn || enemiesMoving || doingSetup)
			return;

		StartCoroutine (MoveEnemies ());
	}

	public void AddEnemyToList(Enemy script)
	{
		enemies.Add (script);
	}

	IEnumerator MoveEnemies()
	{
		enemiesMoving = true;
		if (enemies.Count == 0) 
		{
			yield return new WaitForSeconds (turnDelay + extraDelay);
		}
		for (int i = 0; i < enemies.Count; i++) 
		{
			enemies [i].MoveEnemy ();
			yield return new WaitForSeconds (enemies [i].moveTime + extraDelay);
		}

		playersTurn = true;
		enemiesMoving = false;

	}
}
